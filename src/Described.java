public class Described extends Place {

    private String description;

    public Described(String name, Category category, int x, int y, String description) {
        super(name, category, x, y);
        this.description = description;
    }

    public Described(String name, int x, int y, String description) {
        super(name, x, y);
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return (String.format("Name: %s\nDescription: %s\nCoordinates: %s", getName(), getDescription(), getPosition()));
    }
}