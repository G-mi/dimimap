import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.StrokeType;


public abstract class Place extends Polygon {
//implements Comparable<Place>
    private String name;
    private Category category;
    private int x;
    private int y;
    private Position position;
    private Boolean marked = false;

    public Place(String name, Category category, int x, int y) {
        // TODO add name and description
        super(x, y, x - 15, y - 30, x + 15, y - 30);
        this.x = x;
        this.y = y;
        position = new Position(x, y);
        this.name = name;
        this.category = category;
        setDefaultColours();
        if (category == Category.Bus) {
            setFill(Color.RED);
        } else if (category == category.Underground) {
            setFill(Color.BLUE);
        } else setFill(Color.GREEN);
    }

    // TODO seems like the equals is never called upon

//    @Override
//    public boolean equals(Object other) {
//        System.out.println("I love you too");
//        if (other != null) {
//            Place place = (Place) other;
//            return x == place.x && y == place.y;
//        }
//        return false;
//    }

//    @Override
//    public boolean equals(Place other) {
//        System.out.println("I love you too");
//        if (other instanceof Place){
//            Place p = (Place)other;
//            return x == p.x && y == p.y;
//        } return false;
//    }


    public int hashCode(){
        int tmp = ( y +  ((x+1)/2));
        return x + ( tmp * tmp);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Category getCategory() {
        return category;
    }

    public Position getPosition() {
        return position;
    }

    public String getName() {
        return name;
    }

    public Place(String name, int x, int y) {
        this(name, Category.None, x, y);
        setFill(Color.BLACK);
    }

    public void setDefaultColours() {
        setStroke(Color.BLACK);
        setStrokeType(StrokeType.CENTERED);
        setStrokeWidth(1);
    }

    public void setMarkedColours() {
        setStroke(Color.YELLOW);
        setStrokeType(StrokeType.CENTERED);
        setStrokeWidth(6);
    }

    public void setMark(Boolean mark) {
        if (mark) {
            setMarkedColours();
        } else setDefaultColours();
    }

    public void markSwap() {
        marked = ! marked;
    }

    public boolean getMarkStatus() {
        return marked;
    }

    /*
    TODO this one can still be better. Got 2 misses on a 10000 test
    This worked by I need to understand why the equals method did not work
     */

//    @Override
//    public int compareTo(Place other) {
//        int temp = other.position.getX() * ( y +  ((x+1)/7));
//        return x +  ( this.getPosition().getY() * temp);
//
////        return ((this.position.getX() - other.getPosition().getX()) - (this.position.getY() - other.getPosition().getY()));
//    }
}
