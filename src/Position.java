public class Position {

    private int x;
    private int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public boolean equals(Object other){
        if (other instanceof Position) {
            Position p = (Position) other;
            return x == p.x && y == p.y;
        }
        else
            return false;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
    public int hashCode() {
        int tmp = (y + ((x + 1) / 2));
        return x + (tmp * tmp);
    }

    @Override
    public String toString() {
        return String.format("[%d, %d]", x, y);
    }
}