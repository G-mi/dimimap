import javafx.scene.layout.Pane;

import java.util.*;


public class Data {

    private Map<Position, Place> dataByPosition = new HashMap<>();
    private Map<String, Set<Place>> dataByName = new HashMap<>();
    private Map<Category, Set<Place>> dataByCategory = new HashMap<>();
    private Set<Place> dataMarkedPlaces = new HashSet<>();

    public boolean saved = true;

    public void clear() {
        dataByPosition.clear();
        dataByName.clear();
        dataByCategory.clear();
        dataMarkedPlaces.clear();
    }

    public void setSaved(boolean turn) {
        saved = turn;
    }

    public boolean isSaved() {
        return saved;
    }

    public void printCategory() {
        System.out.println(dataByCategory.get(Category.Bus).size());
        System.out.println(dataByPosition.size());
    }

    public void addPlace(Place place) {
        addByPosition(place);
        addByName(place);
        addByCategory(place);
        saved = false;
    }

    public void addMarked(Place place) {
        dataMarkedPlaces.add(place);
    }

    public void removeMarked(Place place) {
        dataMarkedPlaces.remove(place);
    }

    public void removeAllMarked(Pane display) {
        if (!dataMarkedPlaces.isEmpty()) {
            for (Place place : dataMarkedPlaces) {
                dataByCategory.get(place.getCategory()).remove(place);
                dataByPosition.remove(place.getPosition());
                dataByName.get(place.getName()).remove(place);
                display.getChildren().remove(place);
            }
            saved = false;
            dataMarkedPlaces.clear();
        }
    }

    public void addByPosition(Place place) {
        dataByPosition.put(place.getPosition(), place);
    }

    public void addByName(Place place) {
        String name = place.getName();
        Set<Place> dataSameName = dataByName.get(name);
        if (dataSameName == null) {
            dataSameName = new HashSet<>();
            dataByName.put(name, dataSameName);
        }
        dataSameName.add(place);
        dataByName.put(name, dataSameName);
    }

    public void addByCategory(Place place) {
        Category category = place.getCategory();
        Set<Place> dataSameCategory = dataByCategory.get(category);
        if (dataSameCategory == null) {
            dataSameCategory = new HashSet<>();
        }
        if (dataSameCategory.contains(place)) {
        }
        dataSameCategory.add(place);

        dataByCategory.put(category, dataSameCategory);
    }

//     Why some return copies and not the original list?
    public Collection<Place> get() {
        if (dataByPosition.isEmpty()) {
            return null;
        } else {
            Collection<Place> allPlaces = dataByPosition.values();
            return allPlaces;
        }
    }

    public Set<Place> get(String name) {
        if (!dataByName.containsKey(name)) {
            return null;
        } else {
            Set<Place> nameList = dataByName.get(name);
            return nameList;
        }
    }

    public boolean positionExists(Position position) {
        return dataByPosition.containsKey(position);
    }

    public Place get(Position position) {
        if (dataByPosition.get(position) == null) {
            return null;
        }
        return dataByPosition.get(position);
    }

    public void clearSelections() {
        if (dataMarkedPlaces != null) {
            for (Place place : dataMarkedPlaces) {
                place.setMark(false);
            } dataMarkedPlaces.clear();
        }
    }

    public void hide() {
        if (dataMarkedPlaces != null) {
            for (Place place : dataMarkedPlaces) {
                place.setVisible(false);
                place.setMark(false);
            }
        }
    }

    public void setVisibility(Place place, boolean setTo) {
        place.setVisible(setTo);
        place.setMark(setTo);
    }

    public void setVisibilityCategory(Category category, Boolean turn) {
        Set<Place> setCatVisible = dataByCategory.get(category);
        if (setCatVisible !=  null) {
            for (Place place : setCatVisible) {
                place.setVisible(turn);
            }
        }
    }

    public void setVisibility(Set<Place> places, boolean changeTo) {
        for (Place place : places) {
            setVisibility(place, changeTo);
            System.out.println("At Data");
            if (!changeTo && !dataMarkedPlaces.isEmpty()) {
                dataMarkedPlaces.remove(place);
            }
            else {
                dataMarkedPlaces.add(place);
            }
        }
    }

    public boolean placeNameExists(String input) {
        return dataByName.containsKey(input);
    }

    public boolean noMarkedPlaces() {
        return dataMarkedPlaces.isEmpty();
    }

    public boolean isEmpty() {
        return dataByName.isEmpty();
    }
}



/*
TODO
    Search by:

 */
