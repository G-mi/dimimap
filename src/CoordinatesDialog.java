import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class CoordinatesDialog extends Alert {
    private TextField xField = new TextField();
    private TextField yField = new TextField();

    public CoordinatesDialog() {
        super(AlertType.CONFIRMATION);
        setTitle("Provide Coordinates");
        GridPane grid = new GridPane();
        grid.addRow(0, new Label("Coordinate x: "), xField);
        grid.addRow(1, new Label("Coordinate y: "), yField);
        getDialogPane().setContent(grid);
        setHeaderText(null);
    }

    public int getXField() {
        return Integer.parseInt(xField.getText());
    }

    public int getYField() {
        return Integer.parseInt(yField.getText());
    }
}
