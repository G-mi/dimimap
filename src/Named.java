public class Named extends Place {

    public Named(String name, Category category, int x, int y) {
        super(name, category, x, y);
    }

    public Named(String name, int x, int y) {
        super(name, x, y);
    }

    @Override
    public String toString() {
        return (String.format("Name: %s\nCoordinates: %s", getName(), getPosition()));
    }
}
