import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class DescribedDialog extends Alert {
    private TextField nameField = new TextField();
    private TextField descriptionField = new TextField();

    public DescribedDialog() {
        super(AlertType.CONFIRMATION);
        setTitle("Provide name & description");
        GridPane grid = new GridPane();
        grid.addRow(0, new Label("Name: "), nameField);
        grid.addRow(1, new Label("Description: "), descriptionField);
        getDialogPane().setContent(grid);
        setHeaderText(null);
    }

    public String getName() {
        return nameField.getText();
    }

    public String getDescriptionField() {
        return descriptionField.getText();
    }
}
