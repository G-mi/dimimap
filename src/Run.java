import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.*;
import java.util.*;

public class Run extends Application {
    private static final int BORDER_SIZE = 7;
    private static final int PADDING_SMALL = 2;
    private static final int LIST_VIEW_HEIGHT = 80;
    private static final int LIST_VIEW_WIDTH = 120;
    private static final int PRIMARY_STAGE_X_ALIGNMENT = 460;
    private static final int PRIMARY_STAGE_Y_ALIGNMENT = 50;
    private static final int MAX_DESCRIPTION_CHARACTERS = 150;

    private static final String NO_MAP_ERROR = "Load map a first!";
    private static final String POSITION_TAKEN_ERROR = "This position is taken!";
    private static final String EMPTY_NAME_FIELD_ERROR = "Name field cannot be empty!";
    private static final String COMMAS_ERROR = "No commas allowed!";
    private static final String EMPTY_DESCRIPTION_FIELD_ERROR = "Description field cannot be empty!";
    private static final String MAX_DESCRIPTION_CHARACTERS_ERROR = "You exceeded maximum 150 characters!";
    private static final String SEARCH_FIELD_EMPTY_ERROR = "The search field is empty!";
    private static final String NO_PLACES_REGISTERED_ERROR = "No places registered to search for!";
    private static final String WRONG_DATA_TYPE_ERROR = "Wrong input, provide a number!";
    private static final String NO_MARKED_PLACES_ERROR = "There no marked places!";

    private static final String INFO = "Info";
    private static final String NO_PLACES_FOUND_INFO = "No places found!";


    private static final String LOAD_PLACES_WITHOUT_SAVING_TITLE = "Load places without saving?";
    private static final String LOAD_MAP_WITHOUT_SAVING_TITLE = "Load map without saving?";
    private static final String PROCEED_WITHOUT_SAVING_WARNING = "There are unsaved changes.\nDo you still want proceed without saving?";

    private Stage primaryStage;

    private VBox mainVBoxPane;
    private BorderPane myBorderPane;
    private Pane mapDisplay;
    private Boolean mapIsLoaded = false;
    private Image mapImage;

    private RadioButton namedButton;
    private RadioButton describedButton;
    private TextField searchField;
    private Button hideCategoryButton;

    private ListView<Category> categoryList;

    private Data data;

    private ImageView imageView = new ImageView();


    @Override
    public void start(Stage primaryStage) {
        primaryStageSetUp(primaryStage);
        showPrimaryStage(primaryStage);
    }
    private void primaryStageSetUp(Stage primaryStage) {
        this.primaryStage = primaryStage;
        primaryStage.setTitle("Dimi's Map App");

        data = new Data();

        mainVBoxPane = new VBox();
        myBorderPane = new BorderPane();

        mapDisplay = new Pane();
        myBorderPane.setCenter(mapDisplay);

        fileButtonSetUp();
        horizontalBarSetUp();
        verticalBarSetUp();
    }
    private void showPrimaryStage(Stage primaryStage) {
        mainVBoxPane.getChildren().add(myBorderPane);
        Scene scene = new Scene(mainVBoxPane);
        primaryStage.setX(PRIMARY_STAGE_X_ALIGNMENT);
        primaryStage.setY(PRIMARY_STAGE_Y_ALIGNMENT);

//        testPins();
        primaryStage.setOnCloseRequest(new ExitHandler());
//        primaryStage.onCloseRequestProperty();
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    private void fileButtonSetUp() {
        MenuBar menuBar = new MenuBar();
        mainVBoxPane.getChildren().add(menuBar);
        Menu fileMenu = new Menu("File");
        MenuItem loadMapButton = new MenuItem("Load Map");
        loadMapButton.setOnAction(new loadMapHandler());
        MenuItem loadPlacesButton = new MenuItem("Load Places");
        loadPlacesButton.setOnAction(new LoadPlacesHandler());
        MenuItem saveButton = new MenuItem("Save");
        saveButton.setOnAction(new SaveHandler());
        MenuItem exitButton = new MenuItem("Exit");
        exitButton.setOnAction(new ExitButtonHandler());
        menuBar.getMenus().add(fileMenu);
        fileMenu.getItems().addAll(loadMapButton, loadPlacesButton, saveButton, exitButton);

//        mapLoaderTesting();

//        loadMapButton.setOnAction(new loadMapHandler());

        //TODO
        // loadPlacesButton
        // saveButton
        // exitButton
    }
    private void horizontalBarSetUp() {
        GridPane horizontalGridBox = new GridPane();
        horizontalGridBox.setHgap(10);
        horizontalGridBox.setPadding(new Insets(BORDER_SIZE));

        VBox radioButtons = new VBox();
        radioButtons.setAlignment(Pos.CENTER_LEFT);

        namedButton = new RadioButton("Named");
        describedButton = new RadioButton("Described");
        ToggleGroup group = new ToggleGroup();
        group.getToggles().addAll(namedButton, describedButton);
        namedButton.setSelected(true);
        namedButton.setPadding(new Insets(PADDING_SMALL));
        describedButton.setPadding(new Insets(PADDING_SMALL));
        radioButtons.getChildren().addAll(namedButton, describedButton);

        Button newButton = new Button("New");
        newButton.setOnAction(new NewPlaceHandler());


        searchField = new TextField();

        Button searchButton = new Button("Search");
        searchButton.setOnAction(new SearchButtonHandler());
        Button hideButton = new Button("Hide");
        hideButton.setOnAction(new HideButtonHandler());
        Button removeButton = new Button("Remove");
        removeButton.setOnAction(new RemoveButtonHandler());
        Button coordinatesButton = new Button("Coordinates");
        coordinatesButton.setOnAction(new CoordinatesButtonHandler());

        searchField.setPromptText("Search");

        horizontalGridBox.addColumn(0,newButton);
        horizontalGridBox.addColumn(1,radioButtons);
        horizontalGridBox.addColumn(2,searchField);
        horizontalGridBox.addColumn(3,searchButton);
        horizontalGridBox.addColumn(4,hideButton);
        horizontalGridBox.addColumn(5,removeButton);
        horizontalGridBox.addColumn(6,coordinatesButton);

        FlowPane hPane = new FlowPane();
        hPane.getChildren().add(horizontalGridBox);
        hPane.setAlignment(Pos.TOP_CENTER);
        myBorderPane.setTop(hPane);
    }
    private void verticalBarSetUp() {
        GridPane verticalGridBox = new GridPane();
        verticalGridBox.setAlignment(Pos.CENTER_RIGHT);
        verticalGridBox.setPadding(new Insets(BORDER_SIZE));

        Label categoryLabel = new Label("Categories");
        hideCategoryButton = new Button("Hide Category");
        hideCategoryButton.setOnAction(new HideCategoryHandler());


        ObservableList<Category> categories = FXCollections.observableArrayList(Category.Bus, Category.Underground, Category.Train);
        categoryList = new ListView<>(categories);

        categoryList.getSelectionModel().selectedItemProperty().addListener(new ViewByCategoryHandler());


        categoryList.setOrientation(Orientation.VERTICAL);
        categoryList.setMaxHeight(LIST_VIEW_HEIGHT);
        categoryList.setMaxWidth(LIST_VIEW_WIDTH);

        verticalGridBox.addRow(0, categoryLabel);
        verticalGridBox.setHalignment(categoryLabel, HPos.CENTER);
        verticalGridBox.addRow(1, categoryList);
        verticalGridBox.setHalignment(categoryList, HPos.CENTER);
        verticalGridBox.addRow(2, hideCategoryButton);
        verticalGridBox.setHalignment(hideCategoryButton, HPos.CENTER);

        verticalGridBox.setVgap(BORDER_SIZE);
        verticalGridBox.setPadding(new Insets(BORDER_SIZE));

        myBorderPane.setRight(verticalGridBox);
    }
    public static void main(String[] args) {
        launch(args);
    }
    class placeCreationHandler implements EventHandler<MouseEvent> {
        @Override
        public void handle(MouseEvent event) {

            double doubleX = event.getX();
            double doubleY = event.getY();
            int x = (int) doubleX;
            int y = (int) doubleY;
            Position position = new Position(x, y);
            createPin(position);
            restoreCursor();
        }
    }
    class NewPlaceHandler implements EventHandler<ActionEvent>  {
        @Override
        public void handle(ActionEvent event) {
            if (!mapIsLoaded) {
                errorAlertManager(NO_MAP_ERROR);
            } else {
                mapDisplay.setCursor(Cursor.CROSSHAIR);
                mapDisplay.setOnMouseClicked(new placeCreationHandler());
            }
        }
    }
    private void createPin(Position position) {
        if (data.positionExists(position)) {
            errorAlertManager(POSITION_TAKEN_ERROR);
        }
        Place place;
        if (namedButton.isSelected()) {
            place = namedPlaceCreator(position);
        } else {
            place = describedPlaceCreator(position);
        }
        if (place != null) {
            addPlaceRun(place);
        }
    }
    private Named namedPlaceCreator(Position position) {
        NamedDialog dialog = new NamedDialog();
        Optional<ButtonType> answer = dialog.showAndWait();
        Named place = null;
        if (answer.isPresent() && answer.get() == ButtonType.OK) {
            String name = dialog.getName();
            if (name.isEmpty()) {
                errorAlertManager(EMPTY_NAME_FIELD_ERROR);
                return null;
            }
            if (name.contains(",")) {
                errorAlertManager(COMMAS_ERROR);
                return null;
            }
            if (categoryList.getSelectionModel().isEmpty()) {
                place = new Named(name, position.getX(), position.getY());
            } else {
                place = new Named(name, getCategoryListView(), position.getX(), position.getY());
            }
        } return place;
    }
    private Described describedPlaceCreator(Position position) {
        DescribedDialog dialog = new DescribedDialog();
        Optional<ButtonType> answer = dialog.showAndWait();
        Described place = null;
        if (answer.isPresent() && answer.get() == ButtonType.OK) {
            String name = dialog.getName();
            if (name.isEmpty()) {
                errorAlertManager(EMPTY_NAME_FIELD_ERROR);
                return null;
            }
            if (name.contains(",")) {
                errorAlertManager(COMMAS_ERROR);
                return null;
            }
            String description = dialog.getDescriptionField();
            if (description.isEmpty()) {
                errorAlertManager(EMPTY_DESCRIPTION_FIELD_ERROR);
                return null;
            }
            if (description.contains(",")) {
                errorAlertManager(COMMAS_ERROR);
                return null;
            }
            if (description.length() > MAX_DESCRIPTION_CHARACTERS) {
                errorAlertManager(MAX_DESCRIPTION_CHARACTERS_ERROR);
                return null;
            }
            if (categoryList.getSelectionModel().isEmpty()) {
                place = new Described(name, position.getX(), position.getY(), description);
            } else {
                place = new Described(name, getCategoryListView(), position.getX(), position.getY(), description);
            }
        }
        return place;
    }
    private void addPlaceRun(Place place) {
        place.setOnMouseClicked(new PlaceMarkerHandler());
        data.addPlace(place);
        mapDisplay.getChildren().add(place);
    }
    private Category getCategoryListView() {
        Category category = categoryList.getSelectionModel().getSelectedItem();
        return category;
    }
    private void restoreCursor() {
        mapDisplay.setOnMouseClicked(null);
        mapDisplay.setCursor(Cursor.DEFAULT);
    }
    private void errorAlertManager(String alertMessage) {
        Alert alert = new Alert(Alert.AlertType.ERROR, alertMessage);
        alert.showAndWait();
    }
    private void informationAlertManager(String alertHeader) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(alertHeader);
        alert.setTitle(INFO);
        alert.showAndWait();
    }
    private void informationAlertManager(Place place) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(place.toString());
        alert.setTitle(INFO);
        alert.showAndWait();
    }
    private void mapLoaderTesting() {
        mapImage = new Image("File:/Users/gmi/DSV_SU/Year1/PROG2/jarvafaltet.png");
        imageView.setImage(mapImage);
        mapDisplay.getChildren().add(imageView);
        mapIsLoaded = true;
        primaryStage.sizeToScene();
    }
    private void exitAlerting() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setHeaderText("There are unsaved changes made.\nDo you still want proceed exiting without saving?");
        alert.setTitle("Exit without saving?");
        Optional<ButtonType> answer = alert.showAndWait();
        if (answer.isPresent() && answer.get() == ButtonType.OK) {
            primaryStage.close();
        }
    }

    private Optional<ButtonType> optionalAlerting(String headText, String titleText) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setHeaderText(headText);
        alert.setTitle(titleText);
        Optional<ButtonType> answer = alert.showAndWait();
        return answer;
    }

    private void clearDisplay() {
        data.clear();
        mapDisplay.getChildren().clear();
        mapDisplay.getChildren().add(imageView);
    }

    class loadMapHandler implements EventHandler<ActionEvent> {
        @Override
        public void handle(ActionEvent actionEvent) {
            if (!data.isSaved()) {
                Optional<ButtonType> answer = optionalAlerting(PROCEED_WITHOUT_SAVING_WARNING, LOAD_MAP_WITHOUT_SAVING_TITLE);
                if (answer.isPresent() && answer.get() != ButtonType.OK) {
                    return;
                }
            }
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Load Map");
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("Images", "*.bmp", "*.jpg", "*.jpeg", "*.gif", "*.png"),
                    new FileChooser.ExtensionFilter("All files", "*.*")
            );
            File file = fileChooser.showOpenDialog(primaryStage);
            if (file == null) {
                return;
            }
            String fileName = file.getAbsolutePath();
            if (mapImage != null) {
                mapDisplay.getChildren().remove(imageView);
            }
            mapImage = new Image("File:" + fileName);
            imageView.setImage(mapImage);
            mapDisplay.getChildren().add(imageView);
            mapIsLoaded = true;
            primaryStage.sizeToScene();
            data.setSaved(true);
        }
    }
    // TODO reimplement it without iteration
    class HideCategoryHandler implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {
            Category category = categoryList.getSelectionModel().getSelectedItem();
            data.setVisibilityCategory(category, false);
            categoryList.getSelectionModel().clearSelection();
        }
    }
    class ViewByCategoryHandler implements ChangeListener<Category> {

        @Override
        public void changed(ObservableValue obs, Category oldValue, Category newValue) {
            data.setVisibilityCategory(newValue, true);
        }
    }
    class HideButtonHandler implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {
            data.hide();
        }
    }
    class PlaceMarkerHandler implements EventHandler<MouseEvent> {

        @Override
        public void handle(MouseEvent event) {
            Place place = (Place) event.getSource();
            place.markSwap();
            if (place.getMarkStatus() && event.getButton() == MouseButton.PRIMARY) {
                place.setMark(true);
                data.addMarked(place);
                System.out.println(place.getClass());
            } else if (!place.getMarkStatus() && event.getButton() == MouseButton.PRIMARY) {
                place.setMark(false);
                data.removeMarked(place);
            } else if (place.getMarkStatus() && event.getButton() == MouseButton.SECONDARY) {
                informationAlertManager(place);
            }
        }
    }
    class SearchButtonHandler implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {
            String input = searchField.getText();
            searchField.clear();
            data.clearSelections();
            if (input.isEmpty()) {
                errorAlertManager(SEARCH_FIELD_EMPTY_ERROR);
                return;
            }
            else if (data.isEmpty()) {
                errorAlertManager(NO_PLACES_REGISTERED_ERROR);
                return;
            }
            else if (!data.placeNameExists(input)) {
                informationAlertManager(NO_PLACES_FOUND_INFO);
                return;
            }
            else {
                Set<Place> nameSet = data.get(input);
                data.setVisibility(nameSet, true);
            }
        }
    }
    class CoordinatesButtonHandler implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {
            try {
                CoordinatesDialog dialog = new CoordinatesDialog();
                Optional<ButtonType> answer = dialog.showAndWait();
                if (answer.isPresent() && answer.get() == ButtonType.OK) {
                    Position position = new Position(dialog.getXField(), dialog.getYField());
                    data.clearSelections();
                    if (data.positionExists(position)) {
                        Place place = data.get(position);
                        data.setVisibility(place, true);
                        System.out.println("Love you");
                    } else informationAlertManager(NO_PLACES_FOUND_INFO);
                }
            } catch (NumberFormatException e) {
                errorAlertManager(WRONG_DATA_TYPE_ERROR);
            }
        }
    }
    class RemoveButtonHandler implements EventHandler<ActionEvent> {
        @Override
        public void handle(ActionEvent event) {
            if (data.noMarkedPlaces()) {
                errorAlertManager(NO_MARKED_PLACES_ERROR);
            } else data.removeAllMarked(mapDisplay);
        }

    }
    class ExitButtonHandler implements EventHandler<ActionEvent> {
        @Override
        public void handle(ActionEvent event) {
            if (data.isSaved()) {
                primaryStage.close();
            }
            else {
                exitAlerting();
            }
        }
    }
    class ExitHandler implements EventHandler<WindowEvent> {
        @Override
        public void handle(WindowEvent event) {
            if (data.isSaved()) {
                primaryStage.close();
            }
            else {
                exitAlerting();
                event.consume();
            }
        }
    }

    private void testPins() {

        Random randomX = new Random();
        Random randomY = new Random();

        for (int i = 0; i < 10; i++) {
            int randomIntegerX = randomX.nextInt(920);
            int randomIntegerY = randomY.nextInt(780);

            Position position = new Position(randomIntegerX, randomIntegerY);
            if (!data.positionExists(position)) {
                addPlaceRun(new Named(String.valueOf(i), Category.Bus, randomIntegerX, randomIntegerY));
            }
        } data.printCategory();
    }

    class SaveHandler implements EventHandler<ActionEvent> {
        @Override
        public void handle(ActionEvent event) {
            try {
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Save Places");
                File file = fileChooser.showSaveDialog(null);
                if (file == null)
                    return;
                String fileName = file.getAbsolutePath();
                FileWriter outFile = new FileWriter(fileName);
                PrintWriter out = new PrintWriter(outFile);

                for (Place p : data.get()) {
                    if (p instanceof Described) {
                        out.println(String.format("Described,%s,%d,%d,%s,%s", p.getCategory(), p.getX(), p.getY(), p.getName(), ((Described) p).getDescription()));
                    } else {
                        out.println(String.format("Named,%s,%d,%d,%s", p.getCategory(), p.getX(), p.getY(), p.getName()));
                    }
                }
                data.setSaved(true);
                out.close();
            } catch (FileNotFoundException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR, "Cannot open file" + e.getMessage());
                alert.showAndWait();
            } catch(IOException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR, e.getMessage());
                alert.showAndWait();
            }
        }
    }
    class LoadPlacesHandler implements EventHandler<ActionEvent> {
        @Override
        public void handle(ActionEvent event) {
            if (!mapIsLoaded) {
                errorAlertManager(NO_MAP_ERROR);
                return;
            }
            if (mapIsLoaded && !data.isSaved()) {
                Optional<ButtonType> answer = optionalAlerting(PROCEED_WITHOUT_SAVING_WARNING, LOAD_PLACES_WITHOUT_SAVING_TITLE);
                if (answer.isPresent() && answer.get() != ButtonType.OK) {
                    return;
                }
            }
            try {
                clearDisplay();
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Load places");
                File file = fileChooser.showOpenDialog(null);
                if (file == null)
                    return;
                String fileName = file.getAbsolutePath();

                FileReader inFile = new FileReader(fileName);
                BufferedReader in = new BufferedReader(inFile);
                String line = null;
//                if (line == null || line.length() > 6 || line.length() < 5) {
//                    errorAlertManager("Wrong type of file!");
//                    return;
//                }
                while ((line = in.readLine()) != null) {
                    String[] tokens = line.split(",");
                    int x = Integer.parseInt(tokens[2]);
                    int y = Integer.parseInt(tokens[3]);
                    String name = tokens[4];
                    Category category = Category.valueOf(tokens[1]);
                    Place place;
                    if (tokens[0].equalsIgnoreCase("Named")) {
                        if (!category.equals(Category.None)) {
                            place = new Named(name, category, x, y);
                        } else place = new Named(name, x, y);
                    } else {
                        if (!category.equals(Category.None)) {
                            place = new Described(name, category, x, y, tokens[5]);
                        } else place = new Described(name, x, y, tokens[5]);
                    }
                    addPlaceRun(place);
                }
                in.close();
                inFile.close();
            } catch (FileNotFoundException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR, "Cannot open file: " + e.getMessage());
                alert.showAndWait();
            } catch (IOException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR, "IO-error: " + e.getMessage());
                alert.showAndWait();
            }
        }
    }
}

/*
TODO today!
    1. Not allowing places to load before a map is loaded
    2. Not allowing to load two places at once.
    3. Emptying the data structures before loading new places.
    4. Implement optional alerting method
    5. When trying to load places and feed it a picture instaed: Exception in thread "JavaFX Application Thread" java.lang.ArrayIndexOutOfBoundsException: Index 2 out of bounds for length 1
    6. Load Map after loading places
 */


/*
 TODO Do not forget to put the loadHandler back on
 */